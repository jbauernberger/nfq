# nfq: intercept dns-packets and block IDN (punicode) domains with NFQUEUE

Block all punicode domain names (containing the string `xn--`) to mitigate [homograph (phishing) attacks](https://en.wikipedia.org/wiki/IDN_homograph_attack). 

- nfq is for people who have no real use for IDN/punicode domains. 
- nfq is written to block out all punicode dns requests/answers from or to a local resolver (e.g. unbound or dnsmasq on port 127.0.0.1:53)
- nfq allows whitelisting of **selected** domains only. 
- nfq therefore makes the assumption that you trust your "local" dns as well as your configured forwarding (upstream) resolvers MORE than what browser vendors "think is good for you". 

## Motivation

For a while I used a [patched dnsmasq](https://github.com/spacedingo/dnsmasq-regexp_2.76) which allows sink-holing any punicode domains. Such regex filtering is available on pi-hole's [fork of dnsmasq](https://github.com/pi-hole/FTL), but unfortunately this feature isn't available in upstream dnsmasq (and probably never will be). 

Because it isn't feasable to backport these regex patches on every new dnsmasq release, another solution is needed. 

### Note to DoH users

Firefox now by default ships with DoH and so bypasses the local resolvers (including your cache, and `/etc/hosts` blocklist). 

Whether using DoH is a good idea depends on whether you [trust your DNS provider](https://www.icsi.berkeley.edu/pubs/networking/redirectingdnsforads11.pdf), and whether you're OK with DoH passing all your traffic to Cloudflare (based in the US) or NextDNS (based in France) as is default with Firefox now.

While this is great for the average Joe who otherwise would leak DNS to their ISP (who resells the data), this approach throws anyone under the bus who actually trusts their DNS provider (more than they trust Clownflare/NextDNS). This software is for people who are in the latter camp!

In browsers such as firefox, DoH needs to be [disabled](https://support.mozilla.org/en-US/kb/firefox-dns-over-https#w_manually-enabling-and-disabling-dns-over-https) (or just edit `about:config` and then set `network.trr.mode` to `5`)

When running your own resolver, disabling DoH in Firefox is a good idea anyway because it means that DoH would bypass your `/etc/hosts` blacklist and local dns caching would also be ignored. 

In any case `nfq` assumes a local caching resolver (dnsmasq/unbound/etc) is accepting connections on UDP port 53. For `nfq` to work an iptables rule needs to be configured as described below. 

Of course you can still use DoH with the upstream resolvers, nfq doesn't interfere here and this note is only to make you aware that recent browsers will bypass your local resolvers

## Installation

Install build dependencies:

In case you don't have `make` and a C compiler (gcc, clang, etc) on your system, install it first with `sudo apt-get install build-essential`. Then:

```
sudo apt-get install libnfnetlink-dev \
                     libnfnetlink0 \
                     libnetfilter-queue-dev \
                     libnetfilter-queue1 \
                     libcap-ng-utils \
                     libcap-ng-dev \
                     libcap-ng0 \
                     libbsd-dev \
                     libbsd0   

```

Note: `libcap-ng0` is [a small wrapper](https://people.redhat.com/sgrubb/libcap-ng/) around the functions in [linux/capability.h](https://github.com/torvalds/linux/blob/master/include/uapi/linux/capability.h) which allows us to drop capabilities in just 3 lines of code! nfq requires these capabilities to function: `net_admin`, `sys_nice`, `sys_resource`.

There is no `configure` step in the build process (yet). Just run `make` to build the nfq binary.

Note on build errors. The build will treat warnings as errors. So chances are high that things fail on your end (sorry). In that case please remove the `-Werror` switch in the `Makefile`, and retry running `make`. (please raise an issue to let me know about any warnings or errors - thanks :))

In case you get compilation errors due to unsupported hardening flags either install a newer version of gcc or comment out the `CFLAGS_HARD` in `src/Makefile`.

When installing from source run `make install-local` and (!!) **not** `make install` which is only used for debian apt packaging. 

If there were no errors run `sudo make install-local` to copy the `nfq` binary to `/usr/local/sbin/`. Also systemd unit file will be configured so nfq starts automatically on boot. There are also a regular install/uninstall Make target defined in the Makefiles. These are used by the Debian packaging process and so will probably be incomplete for a manual installation (because systemd and apparmor won't be restarted and also the installation will end up in `/usr/sbin` instead of `/usr/local/sbin`, etc ...) as outlined next:

`sudo make install-local` copies the following files to your machine:

```
/usr/local/sbin/nfq                       <-- nfq main executable
/etc/nfq/whitelist.sample                 <-- sample whitelist (not active by default)
/etc/init.d/nfq                           <-- init.d startup script
/etc/systemd/system/nfq.service           <-- systemd service unit file
/etc/default/nfq                          <-- default options passed to nfq
/etc/apparmor.d/usr.local.sbin.nfq        <-- nfq apparmor profile
/etc/apparmor.d/local/usr.local.sbin.nfq  <-- nfq apparmor profile (user edits)
/usr/local/share/man/man8/nfq.8.gz        <-- nfq man page
```

`sudo make install` however copies the following files to your machine:

```
/usr/sbin/nfq                       <-- nfq main executable
/etc/nfq/whitelist.sample           <-- sample whitelist (not active by default)
/etc/init.d/nfq                     <-- init.d startup script
/lib/systemd/system/nfq.service     <-- systemd service unit file
/etc/default/nfq                    <-- default options passed to nfq
/etc/apparmor.d/usr.sbin.nfq        <-- nfq apparmor profile
/etc/apparmor.d/local/usr.sbin.nfq  <-- nfq apparmor profile (user edits)
/usr/share/man/man8/nfq.8.gz        <-- nfq man page
```

For cross-compilation you will need to modify/port the Makefile because currently it uses `-march=native` to optimize for the local machine you're compiling at. 

After make install-local, you MUST MANUALLY create the iptables rule as discussed in the Setup section below. The iptables values from the parameters `--queue-num` and `--dport` must match the `--queue-num` and `--port`) from `/etc/default/nfq`. In case these values are not specified then nfq defaults to `--port 53` and `--queue-num 0`. nfq won't work if it's started with incorrect values not matching your configured iptables.

Verify that nfq is running: `sudo servicectl status nfq.service`
In case it isn't running check if you can start it from the build directory, e.g. `sudo ./nfq --log-level info ` (use `--help` for a full list of options)  

### Debian packaging

To create a .deb package run `make package` which creates a `nfq_$(VERSION)_amd64.deb` in the parent directory which can be install with `dpkg`. Warning: the deb packaging is currently experimental.

### Uninstall 

To uninstall nfq simply run `sudo make uninstall-local` from the build directory, or
manually remove the above mentioned files. 

Don't forget to flush `iptables -F` (or remove the rule from `/etc/firewall.conf` if this is where you keep it). If NFQUEUE is active and no nfq process handles the kernel queue, your DNS rquests will time out.

Note: the `make uninstall` target will not remove the correct files so you should use `make uninstall-local` (as explained in the previous section).

## Setup

ensure nfnetlink_queue kernel module is loaded:

`modprobe nfnetlink_queue`

Crate an `iptables` rule to intercept DNS traffic so `nfq` can inspect it in user-land. For help configuring iptables on Debian and have them persistent across reboots, please see this [post](https://major.io/2009/11/16/automatically-loading-iptables-on-debianubuntu/). The command is:

`sudo iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-num 0`

We will now be able to filter any repiles from the resolver by listening to queue number 0:

```
nfq --syslog --log-facility LOG_USER --log-level notice --port 53 --queue-num 0 --rewrite-answer
```

In case you wish to use a different port in your iptables rule you can do so, by editing the parameters in `/etc/default/nfq` so that nfq is started with the correct `--port <foo>` option. For a full list of options available that you can pass to `/etc/default/nfq` (NFQ_OPTIONS) run `nfq -h`. 

In case you change /etc/default/nfq you must run `sudo systemctl restart nfq.service` for the changes to take effect. Then verify with `sudo systemctl status nfq.service` to ensure everything works as expected.

```
Usage: nfq [OPTION]...
	-h|--help => print this help and exit
	-e|--examples => print some common examples and exit
	-s|--syslog => send logs to syslog instead stdout/stderr (default: no)
-f|--facility <syslog facility> => ONLY used in combination with --syslog (default: LOG_USER) - see man rsyslog.conf(5) and/or syslog(3)
	-l|--log-level <debug|info|notice|warn|error> => log granularity (default: warn)
	-d|--dryrun => Only log DNS packets containing IDN/punicode (default: no)
	-r|--renice <number> => set scheduling priority to value in range -19 to 20 (see 'man 1 renice') (default: not set)
	-q|--queue-num <number> => kernel NFQUEUE number as passed to iptables (default: 0)
	-p|--port <number> => port number of resolver as was passed to iptables (default: 53)
	-w|--whitelist <string> => path to optional whitelist (default: /etc/nfq/whitelist)

DNS rewriting options when NFQ intercepts answers from resolver (iptables --sport 53 ...):
	-a|--rewrite-answer => Rewrite DNS response with 0.0.0.0, and reinject packet (NF_ACCEPT) instead dropping (NF_DROP) packet. Use when intercepting responses. (default: no)
	-4|--rewrite-A-record <ipv4 address> => rewrite destination IP in A records to x.x.x.x (default: 0.0.0.0)
	-6|--rewrite-AAAA-record <ipv6 address> => rewrite destination IP in AAAA records to xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx (default: ::/0)
```

### Common Scenarios / Examples

Passing the `--examples` will print common 5 configuration scenarios along with the corresponding iptables rules.

```
./nfq --examples
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Examples of common configuration scenarios ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[Scenario 1]:
  listen for DNS replies from resolver on port 53.
  modify DNS A and AAAA records (to point to 0.0.0.0 and ::

iptables needs these rules:
root#> iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-num 0

And matching nfq start options:
#root> /usr/local/sbin/nfq --syslog \
		--facility LOG_DAEMON
		--log-level notice --port 53 \
		--queue-num 0 \
		--rewrite-answer

[Scenario 2]:
 - Listen for DNS replies from port 53 from queue number 0.
 - Modify type A and AAAA records to point to 192.168.0.12 and ::ffff:c0a8:c

iptables needs these rules:
root#> iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-num 0

And matching nfq start options:
root#> /usr/local/sbin/nfq --syslog \
		--facility LOG_DAEMON \
		--log-level notice --port 53 \
		--queue-num 0 \
		--rewrite-A-record 192.168.0.12 \
		--rewrite-AAAA-record ::ffff:c0a8:c

[Scenario 3]:
 - Listen for DNS queries going to 53 and placed in queue number 0.
 - Drop (NF_DROP) any packets not whitelisted

iptables needs these rules:
root#> iptables -A INPUT -p udp -m udp --dport 53 -j NFQUEUE --queue-num 0

And matching nfq start options:
root#> /usr/local/sbin/nfq --syslog \
		--facility LOG_DAEMON \
		--log-level notice --port 53 \
		--queue-num 0 \
		--whitelist /etc/nfq/my-whitelist.txt

[Scenario 4]:
 - Listen for DNS replies from port 53 and placed in queue number 0.
 - Do not mangle or drop packets and log matches only

iptables needs these rules:
root#> iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-num 0

And matching nfq start options:
root#> /usr/local/sbin/nfq --syslog \
		--facility LOG_DAEMON \
		--log-level notice --port 53 \
		--queue-num 0 \
		--dryrun

[Scenario 5]:
 - Listen for DNS replies from port 53 and placed in queue number 0, 1, and 2.
 - Modify type A and AAAA records to point to the default sinkholes (0.0.0.0 and ::)

iptables needs these rules:
root#> iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-num 0:2 --queue-balance

And start 1 nfq process for each queue (see also nfq-balance.sh script):
root#> /usr/local/sbin/nfq --syslog \
		--facility LOG_DAEMON \
		--log-level notice --port 53 \
		--queue-num 0 \
		--rewrite-answer
root#> /usr/local/sbin/nfq --syslog \
		--facility LOG_DAEMON \
		--log-level notice --port 53 \
		--queue-num 1 \
		--rewrite-answer
root#> /usr/local/sbin/nfq --syslog \
		--facility LOG_DAEMON \
		--log-level notice --port 53 \
		--queue-num 2 \
		--rewrite-answer
root#> /usr/local/sbin/nfq --syslog \
		--facility LOG_DAEMON \
		--log-level notice --port 53 \
		--queue-num 3 \
		--rewrite-answer
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

```

The default installation assumes you are running nfq on the OUTPUT and using iptables --sport 53 so that the queue
will be filled with UDP DNS answers from the resolver. Intercepting the answer, we can replace (rewrite) the ip 
address of the IDN domain with 0.0.0.0. This will then behave the same way as an entry in `/etc/hosts` (e.g. 
0.0.0.0) would behave.

It's possible to intercept outgoing DNS (questions to the resolver) and deny 
them. However there is no way for us to rewrite / modify anything at the request 
stage (clients check if the answer section matches what they asked for. And 
blocking outgoing requests means that these packets will simply be dropped 
(NF_DROP). To a user this is hard to distinguish from genuine network issues. 
E.g. a link is clicked containing an IDN domain, the client will try to resolve 
the domain but the DNS resolution times out (after a few retries). This can 
take up to half a minute or more. Is is perfectly fine to use this mode since
 for single user system who wants to prevent any DNS resolution done on IDN 
domains and is aware that nfq is running in this mode..

Most people will want to process replies (DNS answers), since this gives you
various rewriting options. To activate rewriting nfq is started with 
`--rewrite-answers`. This setting will rewrite all matching DNS answers and 
sinkhole non-whitelisted IDN domain so that they resolves to 0.0.0.0 (::/0).

In case you're running this on a gateway and are serving many different users
you might want to explain to them why their requests are sinkholed to 0.0.0.0.
Passing `--rewrite-A-record 192.168.0.12` and `--rewrite-AAAA-record ::ffff:c0a8:c`
options will instruct nfq to rewrite the UDP packet DNS payload so that if it
is an A record being returned to send it to an internal host 192.168.0.12 or
another ipv4 address of your choice. You will also want to set the
corresponding ipv6 address (with `--rewrite-AAAA-record`), otherwise you may
get strange results depending on if the A or AAAA record arrives first. I
repeat these 2 options need to be set together unless you know what you're
doing :)


## Dryrun / Test-mode

Use `--dryrun` to log all domains that contain IDN/punicode but do not drop the packets. This can be useful to build a whitelist (from the log data) before activating the default behavior (`NF_DROP` the packets).

To get a list of (unique) domains which would have been blocked (when not using `--dryrun`), run something like:

```
sudo grep 'WARNING: \*\*\*DRYRUN\*\*\* NOT Rejecting IDN domain' /var/log/messages | cut -d ' ' -f13|uniq

```

After verifying the domains on the list, you can then add them to the whitelist.

## Logging

Unless the `--syslog` option is used, all logs are written to stdout/stderr. This is usually picked up by systemd-journald anyway. Using the
`--syslog` option will instead directly write the info to syslog.

Log granularity is controlled via the following log levels: `debug`, `info`, `warn`, `error`, which are passed with the --log-level option, e.g.: `--log-level info`.

The syslog "log facility" defaults to LOG_USER, but can be changed with the `--facility` cmd line parameter, e.g. `--facility LOG_DAEMON`. Check your rsyslogd installation to use something that makes sense for you. In case you set --facility but forget to use --syslog then the --facility option will be ignored.

## Whitelist

nfq tells the kernel to either to rewrite (`--rewrite-address`, `--rewrite-A-record`, and `--rewrite-AAAA-record` options) payloads in UDP responses or to drop the packet which contain IDN/punicode. To selectively whitelist IDN domains create a file named `/etc/nfq/whitelist`, and add any domains which you want to unblock. Wildcards are NOT accepted so all subdomains must be listed too. Take a look at `/etc/nfq/whitelist.sample`. Domains which are listed but do not contain `xn--` (e.g. _"normal"_, non-IDN domains) will be ignored. 

By default there is no whitelist installed (in `/etc/nfq/whitelist`) and you need to create this file yourself (take a look at the provided sample in `/etc/nfq/whitelist.sample`). nfq is restricted (by apparmor) from accessing files outside the /etc/nfq/. Any whitelist file needs to be placed in here. You can change this by editing the local/usr.local.sbin.nfq apparmor configuration and add an additional path. 

In case you wish to edit your whitelist you can do so while nfq is running.
After you have done your edit simply run `sudo systemctl reload nfq.service which will signal nfq to reload the whitelist file.

If running without systemd you can also "manually" reload the whitelist by sending a SIGHUP to the nfq process (`sudo kill -HUP <pid>`) which under the hood is also what `systemctl reload` does.

## Note

It would be trivial to match other strings rather than `xn--` but I haven't found a reason why this might be a good use-case. It's therefore not possible for now but if you need this don't hesitate and open an issue.

## Performance

nfq should perform well for most desktop like systems. 

If nfq is too slow to digest the packets sent from kernel-space, the socket buffer that we use to enqueue packets may fill up returning `ENOBUFS`. This error is logged and nfq will try to mitigate this during runtime with 2 (automatic) mitigation techniques: 

1) increasing the socket receive buffer size with `nfnl_rcvbufsiz()` until it reaches the value of `UDP_SOCK_RMEM_MAX`, and, ...
2) increasing the kernel nfqueue minimum size with `nfq_set_queue_maxlen()` until we reach `QUEUE_MAXLEN` (defined in `nfq.c`).

This is adjusted incrementally (by doubling the current active value each time, and until we reach values of `QUEUE_MAXLEN` and `UDP_SOCK_RMEM_MAX` respectively):

```
static const uint32_t QUEUE_MINLEN = 4096;              // PAGE_SIZE
static const uint32_t QUEUE_MAXLEN = QUEUE_MINLEN*16;   // 65536

static const uint32_t UDP_SOCK_RMEM_MIN = 3276800;
static const uint32_t UDP_SOCK_RMEM_MAX = UDP_SOCK_RMEM_MIN*8; 
```

### Load Balancing

nfq is a single process (no threads or forks). 

#### sysctl tunables

you may want to increase (in `/etc/sysctl.conf`) default for:

```
net.ipv4.udp_rmem_min=3276800 // man udp
```

#### renice/setpriority()

Tell nfq to set the process priority during startup (passing the --renice command line switch). E.g. `nfq --renice -20` would set nfq to the highest possible scheduling value.

#### taskset

Note: this has not lead to measurable improvements in my tests but ymmv: 

Isolate the nfq process onto a dedicated core ([e.g. taskset -p -c 0 PID to bind nfq process with PID to cpu 0](https://manpages.debian.org/unstable/util-linux/taskset.1.en.html))

#### use --queue-balance with iptables

You can use `iptable --queue-balance 0:3` and then run nfq as a single process for each queue (0,1,2 & 3) by explicitly starting each nfq process for a specific queue. An example itables with queue range of 0-3:

```
iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-balance 0:3
```

or even better:

```
iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-balance 0:3 --queue-cpu-fanout
```

as described [here](https://lwn.net/Articles/544379/)


then for each iptables queue in specified range (0:3) start a separate nfq process. An example of how to do this is shown in the `nfq-balance.sh` script. If you want balancing then you must provide your own _plumbing_ to ensure nfq automatically starts during system boot (tip: use [rc.local](https://www.linuxbabe.com/linux-server/how-to-enable-etcrc-local-with-systemd)). You can also create individual systemd unit service files for each nfq process that handles 1 queue each. 

The `nfq-balance.sh` script requires 1 argument, which is the queue range that we used in the above iptables example (0:3) and started like so (as root):

```
sudo ./nfq-balance.sh 0:3
```

This will start a dedicated nfq process for each queue. Sample output:

```
Starting nfq to listen on queue number 0:
/usr/local/sbin/nfq --syslog --log-level info --facility LOG_DAEMON --renice -10 --rewrite-answer --queue-num 0 &> /dev/null &
Starting nfq to listen on queue number 1:
/usr/local/sbin/nfq --syslog --log-level info --facility LOG_DAEMON --renice -10 --rewrite-answer --queue-num 1 &> /dev/null &
Starting nfq to listen on queue number 2:
/usr/local/sbin/nfq --syslog --log-level info --facility LOG_DAEMON --renice -10 --rewrite-answer --queue-num 2 &> /dev/null &
Starting nfq to listen on queue number 3:
/usr/local/sbin/nfq --syslog --log-level info --facility LOG_DAEMON --renice -10 --rewrite-answer --queue-num 3 &> /dev/null &

```

You can play with these parameters and see immediate stats on how the settings effect throughput using [dnsblast](https://github.com/jedisct1/dnsblast) (e.g. `dnsblast 127.0.0.1 10000 100`) and see what makes most sense for system. 

TIP: In case you want to signal a group (all) of nfq processes to reload their whitelist you can do:

```
sudo kill -HUP `sudo pgrep nfq`
```

Now we see queues 0-3 active (first field is the queue number):

```
sudo cat /proc/net/netfilter/nfnetlink_queue 
    0   5707     0 2 65531     0     0        0  1
    1   5708     0 2 65531     0     0       12  1
    2   5709     0 2 65531     0     0      205  1
    3   5710    80 2 65531     0     0      292  1
```

The meaning of these fields is explained [here](https://home.regit.org/netfilter-en/using-nfqueue-and-libnetfilter_queue/)

This is used to print statistics, when nfq receives a SIGHUP, and during exit (requires minimum `--log-level notice`), e.g.:

`STATS: queue_total=0 copy_mode=2 copy_range=65531 queue_dropped=0 user_dropped=0`

## Security

by default nfq needs root privilegs due to it's interface to libnetfilter_queue and nfnetlink kernel subsystem. Upon start nfq drops privileges and retains `CAP_SYS_RESOURCE`, `CAP_SYS_NICE`, `CAP_NET_ADMIN`.

An apparmor profile is supplied with the installation and general hardening techniques are applied (see `CFLAGS_HARD` variable in [src/Makefile](src/Makefile) during compilation. Most of the strncpy is replaced via means of libbsd's `strlcpy`.

### hardened_malloc

nfq is compatible with [hardened_malloc](https://github.com/GrapheneOS/hardened_malloc) but it is not enabled in the default installation. See the GrapheneOS [documentation](https://github.com/GrapheneOS/hardened_malloc#traditional-linux-based-operating-systems) for how to install this on your system. 

To link nfq against `hardened_malloc` and assuming your hardened_malloc is installed in `/usr/local/lib/libhardened_malloc.so` simply add the following directive to your `LDFLAGS` in `src/Makefile`:

```
-L/usr/local/lib -lhardened_malloc
```

After the build you should see the shared lib has been linked against the nfq executable:

```
$> ldd src/nfq/src/nfq | grep libhardened_malloc.so
	libhardened_malloc.so => /usr/local/lib/libhardened_malloc.so (0x00007f1999f25000)
```

There is (probably) no need to change the `vm.max_map_count` setting since nfq isn't some forking/multiplexing server and uses the heap very little anyway. 

The apparmor profile shipped with nfq allows the lib to be loaded form /usr/local/lib therefore if you use a different installation path in your local install ensure to specify your path in `apparmor.d/local/usr.local.sbin.nfq`

### Why a whitelist instead of a blacklist

using a blacklist you will never know if there are some phishing domains which you might have missed and phishermen are generally always trying to come up with new tricks. Therefore the only way to be really certain about blocking all homograph attacks is to deny all punicode domains and whitelist only those that you need.

In case you really think that a blacklist is the only way you would want to use nfq in your set-up I won't judge. Send me a pull-request or a email to let me know you want this and I'd be happy to add the code to make this work. In that case you could use something like [dnstwist](https://github.com/elceef/dnstwist) to generate the blacklist for you and then feed that into nfq. But as mentioned I haven't done this because nobody has asked me to (so far) because you could also just simply rely on /etc/hosts instead of using this software.

## Misc / FAQ

### Some requests are not handled by nfq

if you find that requests from the browser are visible when running with `--log-level debug` but not when making queries with dig/nslookup (or vise versa), then most likely there are several nameservers configured inside `/etc/resolv.conf` and nfq then only sees the ones that happened to go to 127.0.0.1. There should be only dnsmasq managing your requests (running on 127.0.0.1). The culprit might be NetworkManager: ensure it doesn't write to your `/etc/resolv.conf`. The content of `/etc/NetworkManager/NetworkManager.conf` would typically look like this (note `dns=none`):

```
cat /etc/NetworkManager/NetworkManager.conf
[main]
plugins=ifupdown,keyfile
dns=none

[ifupdown]
managed=false
```

And your `/etc/resolve.conf` has (only!!):

```
nameserver 127.0.0.1
```

Typically your `/etc/dnsmasq.conf` would look similar to this:

```
port=53
domain-needed
bogus-priv
conf-file=/usr/share/dnsmasq-base/trust-anchors.conf
dnssec
## resolv.dnsmasq has our upstream/forwarding nameservers:
resolv-file=/etc/resolv.dnsmasq

no-poll

# make sure Firefox does not bypass our DNS via DoH
# and/or set network.trr.mode=5 in Firefox about:config
address=/use-application-dns.net/

user=dnsmasq
group=dnsmasq
interface=lo
listen-address=127.0.0.1
no-dhcp-interface=
bind-interfaces
cache-size=50000
dns-forward-max=50
```

### Why is there no TCP support?

strange question, but fwiw, DNS uses TCP only for zone transfers and nfq is oblivious to that. It only processes DNS answers or questions to and from a client and gets out of the way in other scenarios.

### Can I still use DoH?

yes. You might still want to use DoH for outgoing / forwarding queries (e.g. from dnsmasq/unbound to the Internet). nfq does not stop you from doing so. 

## Support

commercial support is available on request. Please get in touch via email.

## Bugs

Probably :)
In case something doesn't make sense please shoot me a message, open an issue or send a pull request. 🙏

