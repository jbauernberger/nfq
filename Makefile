##	 __  __       _         __ _ _      
##	|  \/  | __ _| | _____ / _(_) | ___ 
##	| |\/| |/ _` | |/ / _ \ |_| | |/ _ \
##	| |  | | (_| |   <  __/  _| | |  __/
##	|_|  |_|\__,_|_|\_\___|_| |_|_|\___|
##


all:
	+$(MAKE) release -C src/
	+$(MAKE) debug -C src/

release:
	+$(MAKE) $@ -C src/

debug:
	+$(MAKE) $@ -C src/

clean:
	+$(MAKE) $@ -C src/

distclean:
	+$(MAKE) $@ -C src/

dist:
	+$(MAKE) $@ -C src/

package:
	+$(MAKE) $@ -C src/

install:
	+$(MAKE) $@ -C src/

install-local:
	+$(MAKE) $@ -C src/

uninstall:
	+$(MAKE) $@ -C src/

uninstall-local:
	+$(MAKE) $@ -C src/
