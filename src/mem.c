#include "nfq.h"

/*
 ** allcate and free routines for all dynamic data structures
 */


extern void * xrealloc(void *p, size_t n)
{
    p = realloc(p,n);
    if (p == NULL)
    {
        LERR("%s: Unable to realloc(): %s\n",__func__,strerror(errno));
        exit (EXIT_FAILURE);
    }
    return p;
}

extern void * xmalloc(size_t n)
{
    void *p;
    p = malloc(n);
    if (p == NULL)
    {
        LERR("%s: Unable to malloc(): %s\n",
                __func__, strerror(errno));
        exit (EXIT_FAILURE);
    }
    return p;
}

extern char * xstrdup(char *s)
{
    char *p = NULL;

    if (s == NULL) return s;

    p = strdup(s);
    if (p == NULL)
    {
        LERR("%s: Unable to strdup(): %s\n",__func__,strerror(errno));
        exit(EXIT_FAILURE);
    }
    return p;
}

extern list_t * list_t_malloc(void)
{
    list_t *p, zz = {0};

    p = xmalloc(sizeof (list_t));
    p->str = NULL;
    p->next = NULL;

    *p = zz;

    return p;
}

extern void * opt_cfg_malloc(void)
{
    opt_cfg *c, zz = {0};

    c = xmalloc(sizeof(opt_cfg));

    *c = zz;
    
    c->queue_num = 0;
    c->log_level = LOG_WARNING;
    c->syslog = 0;
    c->syslog_facility = LOG_USER;
    c->port = 53;
    c->dryrun = 0;
    c->renice = 0;

    c->rewrite_answer = 0;
    c->rewrite_A = 0;
    c->rewrite_AAAA = 0;

    memset (c->rewrite_ip_a, 0, sizeof(struct in6_addr));
    memset (c->rewrite_ip_a_str, 0, 16);
    strlcpy(c->rewrite_ip_a_str, 
            CONFIG_DEFAULT_IPV4_INJECT , 
            sizeof(c->rewrite_ip_a_str)-1);
    inet_pton(AF_INET, c->rewrite_ip_a_str, c->rewrite_ip_a);

    memset (c->rewrite_ip_aaaa, 0, sizeof(struct in6_addr));
    memset (c->rewrite_ip_aaaa_str, 0, 40);
    strlcpy(c->rewrite_ip_aaaa_str, 
            CONFIG_DEFAULT_IPV6_INJECT, 
            sizeof(c->rewrite_ip_aaaa_str)-1);
    inet_pton(AF_INET6, c->rewrite_ip_aaaa_str, c->rewrite_ip_aaaa);

    memset (c->whitelist_path,  0, PATH_MAX);
    strlcpy(c->whitelist_path, 
            CONFIG_DEFAULT_WHITELIST_PATH, 
            PATH_MAX-1);

    return c;
}

extern void xfree(void *p)
{
    if (p) 
    {
        free (p);
        p = NULL;
    }
}

extern void opt_cfg_free(opt_cfg *c)
{
    if (c)
    {
        xfree(c);
    }

    return;
}

extern void list_t_free(list_t *l)
{
    if (l)
    {
        list_t *tmp;
        do
        {
            tmp = l->next;
            xfree(l->str);
            xfree(l);
            l = tmp;
        } while (l);
    }
}

extern void free_urls(query_list qlist) 
{
    uint16_t i;
    for (i = 0; i < qlist.n_queries; i++) {
        xfree(qlist.urls[i]);
    }
    xfree(qlist.urls);
}

