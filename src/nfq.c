/*
 * Intercept DNS packets and reject anything that matches regex (useful to
 * sinkhole any queries which contain punicode.
 */

#include "nfq.h"

opt_cfg *cfg = { 0 };
char *pgm = NULL; // becomes argv[0]
pid_t pid;

/* 
 * min / max length ranges for calls to nfq_set_queue_maxlen() 
 * 
 */
static const uint32_t QUEUE_MINLEN = 4096;  
static const uint32_t QUEUE_MAXLEN = QUEUE_MINLEN*16; // 65536

static const uint32_t UDP_SOCK_RMEM_MIN = 3276800; 
static const uint32_t UDP_SOCK_RMEM_MAX = UDP_SOCK_RMEM_MIN*8; // 32768

static list_t * opt_whitelist_ptr = { 0 };
static struct nfq_handle *handle_ptr = { 0 };
static struct nfq_q_handle *q_handle_ptr = { 0 };

static void usage(const int cond)
{
    printf("Usage: %s [OPTION]...\n",pgm);
    printf("\t-h|--help => print this help and exit\n");
    printf("\t-v|--version => print version information and exit\n");
    printf("\t-e|--examples => print some common examples and exit\n");
    printf("\t-s|--syslog => "
            "send logs to syslog instead stdout/stderr (default: no)\n");
    printf("\t-f|--facility <syslog facility> => "
            "ONLY used in combination with --syslog (default: LOG_USER) "
            "- see man rsyslog.conf(5) and/or syslog(3)\n");
    printf("\t-l|--log-level <debug|info|notice|warn|error> => "
            "log granularity (default: warn)\n");
    printf("\t-d|--dryrun => "
            "Only log DNS packets containing IDN/punicode (default: no)\n");
    printf("\t-r|--renice <number> => "
            "set scheduling priority to value in range -19 to 20 "
            "(see 'man 1 renice') (default: not set)\n");
    printf("\t-q|--queue-num <number> => "
            "kernel NFQUEUE number as passed to iptables (default: %d)\n", 
            (int)cfg->queue_num);
    printf("\t-p|--port <number> => "
            "port number of resolver as was passed to iptables (default: %d)\n", 
            (int)cfg->port);
    printf("\t-w|--whitelist <string> => "
            "path to optional whitelist (default: %s)\n\n", 
            CONFIG_DEFAULT_WHITELIST_PATH);

    printf("DNS rewriting options when NFQ intercepts answers from resolver (iptables --sport 53 ...):\n");
    printf("\t-a|--rewrite-answer => "
            "Rewrite IP in DNS answers %s (A records) %s (AAAA records), and reinject "
            "instead dropping  packet. "
            "Use when intercepting responses. (default: no)\n", CONFIG_DEFAULT_IPV4_INJECT,
            CONFIG_DEFAULT_IPV6_INJECT);
    printf("\t-4|--rewrite-A-record <ipv4 address> => "
            "rewrite destination IP in A records to x.x.x.x (default: %s)\n",
            CONFIG_DEFAULT_IPV4_INJECT);
    printf("\t-6|--rewrite-AAAA-record <ipv6 address> => "
            "rewrite destination IP in AAAA records "
            "to xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx (default: %s/0)\n",
            CONFIG_DEFAULT_IPV6_INJECT);

    printf("\n\t;; nfq %s (build_date: %s)\n", VERSION, BUILD_DATE);
    printf("\t;; Home:\t%s\n", DOCUMENTATION_URL);
    printf("\t;; Issues:\t%s/issues\n", DOCUMENTATION_URL);
    opt_cfg_free(cfg);
    exit (cond);
}

static void examples(void)
{
    int x;
    printf(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
    printf(";; Examples of common configuration scenarios ;;\n");
    printf(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n\n");

    printf("[Scenario 1]:\n"
            "  listen for DNS replies from resolver on port 53.\n"
            "  modify DNS A and AAAA records (to point to 0.0.0.0 and ::\n");
    printf("\niptables needs these rules:\n");
    printf("root#> iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-num 0\n\n");
    printf("And matching nfq start options:\n");
    printf("#root> /usr/local/sbin/nfq --syslog \\\n\t\t"
            "--facility LOG_DAEMON\n\t\t"
            "--log-level notice --port 53 \\\n\t\t"
            "--queue-num 0 \\\n\t\t"
            "--rewrite-answer\n\n");

    printf("[Scenario 2]:\n"
            " - Listen for DNS replies from port 53 from queue number 0.\n"
            " - Modify type A and AAAA records to point to 192.168.0.12 and ::ffff:c0a8:c\n");
    printf("\niptables needs these rules:\n");
    printf("root#> iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-num 0\n\n");
    printf("And matching nfq start options:\n");
    printf("root#> /usr/local/sbin/nfq --syslog \\\n\t\t"
            "--facility LOG_DAEMON \\\n\t\t"
            "--log-level notice --port 53 \\\n\t\t"
            "--queue-num 0 \\\n\t\t"
            "--rewrite-A-record 192.168.0.12 \\\n\t\t"
            "--rewrite-AAAA-record ::ffff:c0a8:c\n\n");

    printf("[Scenario 3]:\n"
            " - Listen for DNS queries going to 53 and placed in queue number 0.\n"
            " - Drop (NF_DROP) any packets not whitelisted\n");
    printf("\niptables needs these rules:\n");
    printf("root#> iptables -A INPUT -p udp -m udp --dport 53 -j NFQUEUE --queue-num 0\n\n");
    printf("And matching nfq start options:\n");
    printf("root#> /usr/local/sbin/nfq --syslog \\\n\t\t"
            "--facility LOG_DAEMON \\\n\t\t"
            "--log-level notice --port 53 \\\n\t\t"
            "--queue-num 0 \\\n\t\t"
            "--whitelist /etc/nfq/my-whitelist.txt\n\n");

    printf("[Scenario 4]:\n"
            " - Listen for DNS replies from port 53 and placed in queue number 0.\n"
            " - Do not mangle or drop packets and log matches only\n");
    printf("\niptables needs these rules:\n");
    printf("root#> iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-num 0\n\n");
    printf("And matching nfq start options:\n");
    printf("root#> /usr/local/sbin/nfq --syslog \\\n\t\t"
            "--facility LOG_DAEMON \\\n\t\t"
            "--log-level notice --port 53 \\\n\t\t"
            "--queue-num 0 \\\n\t\t"
            "--dryrun\n\n");

    printf("[Scenario 5]:\n"
            " - Listen for DNS replies from port 53 and placed in queue number 0, 1, and 2.\n"
            " - Modify type A and AAAA records to point to the default sinkholes (0.0.0.0 and ::)\n");
    printf("\niptables needs these rules:\n");
    printf("root#> iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE --queue-num 0:2 --queue-balance\n\n");
    printf("And start 1 nfq process for each queue (see also nfq-balance.sh script):\n");
    for (x=0; x<4; x++)
        printf("root#> /usr/local/sbin/nfq --syslog \\\n\t\t"
               "--facility LOG_DAEMON \\\n\t\t"
                "--log-level notice --port 53 \\\n\t\t"
                "--queue-num %d \\\n\t\t"
                "--rewrite-answer\n", x);

    printf(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
    opt_cfg_free(cfg);
    exit (EXIT_SUCCESS);
}

static void version(void)
{
    printf(";; %s %s (build_date: %s)\n", pgm, VERSION, BUILD_DATE);
    exit (EXIT_SUCCESS);
}

static void print_stats(void)
{
    size_t len = 0;
    ssize_t read;
    FILE *fp;
    char *line = NULL;
    int i = 0;

    /*
     * see:
     * https://insights-core.readthedocs.io/en/latest/shared_parsers_catalog/nfnetlink_queue.html
     */
    int queue_number;
    int peer_portid;
    int queue_total;
    int copy_mode;
    int copy_range;
    int queue_dropped;
    int user_dropped;
    unsigned long id_sequence;
    int one; 
    
    // read whitelist into list
    fp = fopen(PROC_NFNL_QUEUE, "r");
    if (fp == NULL) {
        LNOTICE("Unable to open %s (%s)\n", PROC_NFNL_QUEUE, strerror(errno));
        return;
    }

    while ((read = getline(&line, &len, fp)) != -1) {
        char *p;

        // clean up trailing new line
        if ((p = strchr(line, '\n')) != NULL) *p = '\0';

        (void)sscanf(line, "%d %d %d %d %d %d %d %lu %d",
                &queue_number, &peer_portid, &queue_total,
                &copy_mode, &copy_range, &queue_dropped, 
                &user_dropped, &id_sequence, &one);

        if (queue_number == (int)cfg->queue_num && peer_portid == (int)pid) {

            LNOTICE("STATS: queue_total=%d copy_mode=%d copy_range=%d "
                    "queue_dropped=%d user_dropped=%d id_sequence=%lu\n",
                    queue_total, copy_mode, copy_range, 
                    queue_dropped, user_dropped, id_sequence);

            break;
        }

        i++;
    }

    (void)fclose(fp);
    if (line)
        xfree(line);

    return;
}

static int find_punicode(unsigned char* pkt)
{
    int puni=0, i = 0;
    query_list qlist;
    
    qlist = extract_urls(pkt + sizeof(struct iphdr) + sizeof(struct udphdr));
    for (i = 0; i < qlist.n_queries; i++) {

        LDEBUG("%s: Extracted domain from DNS payload [%d/%d]: %s\n", 
                __func__, i, qlist.n_queries, qlist.urls[i]);

        if (strstr(qlist.urls[i],"xn--") != NULL) {
            int found_whitelist_match = 0;

            LDEBUG("%s: Found DNS packet with punicode/IDN in the domain name\n",__func__);

            if (opt_whitelist_ptr) {
                list_t * curr = opt_whitelist_ptr;

                LDEBUG("%s: Searching for domain in whitelist ...\n",__func__);    
                while (curr) {
                    LDEBUG("%s: Checking List item: %s ...\n",
                            __func__, curr->str);    

                    if (curr->str && (! strcmp(curr->str, qlist.urls[i])) ) {
                        found_whitelist_match++;
                        break;
                    }
                    curr = curr->next;
                }
            }

            if (found_whitelist_match) {
                LINFO("Accepting whitelisted IDN domain: %s\n",
                        qlist.urls[i]);
            } else {
                if (cfg->dryrun)
                    LWARN("WARNING: ***DRYRUN*** NOT Rejecting IDN domain: %s\n",
                            qlist.urls[i]);

                LNOTICE("Rejecting non-whitelisted IDN domain: %s\n",
                        qlist.urls[i]);

                puni++; // found a match
            }
        } 
    }
    free_urls(qlist);

    return puni;
}

static int logger_cb(
        struct nfq_q_handle *q_handle, 
        struct nfgenmsg *nf_msg, 
        struct nfq_data *data, 
        void *cb_data)
{
    struct nfqnl_msg_packet_hdr *nl_hdr = { 0 };
    int id = 0, pkt_sz = 0;
    unsigned char *pkt;
    struct iphdr *ip_hdr = { 0 };
    struct udphdr *udp_hdr = { 0 };

    nl_hdr = nfq_get_msg_packet_hdr(data);
    if (nl_hdr) {
        id = ntohl(nl_hdr->packet_id);
    } else {
        id = 0;
        LWARN("nfq_get_msg_packet_hdr() returned NULL: %s\n", strerror(nfq_errno));
    }

    pkt_sz = nfq_get_payload(data, &pkt);
    if (pkt_sz <0)
        LWARN("nfq_get_payload() returned -1 (%s)\n", strerror(nfq_errno));

    ip_hdr = (struct iphdr*)pkt;

    LDEBUG("%s: Received IP packet size=%d with protocol[%d]\n",
            __func__, pkt_sz, ip_hdr->protocol);

    if (ip_hdr->protocol == IPPROTO_UDP) 
    {
        udp_hdr = (struct udphdr*)(pkt+sizeof(struct iphdr));

        /* We are here because:
         * iptables -A INPUT -p udp -m udp --dport 53 -j NFQUEUE 
         *    --> E.g. we see UDP **request** packets (containing DNS questions)! 
         *
         * E.g. if we see DNS **request** packets, there is no point in modifing/mangling
         * the packet. Modifying the "question" here would get detected by the client
         * which always checks if what they received matches what they asked
         * for. dig/nslookup throw an error saying "this isn't the location I
         * asked for" (or something to that effect). 
         *
         * If the user wants to reject DNS requests (questions), all we can offer is 
         * to tell the kernel to NF_DROP the packet. This is actually a
         * very effective method as long as UserExperience[tm] doesn't matter.
         * To the user the request will seem to stall until it runs into a timeout
         * (after several retransmissions of the packet). This is because the 
         * kernel never forwards it to the resolver. All a user can see
         * is that the request eventually timed out without any inidcation as to why.
         * It is indistinguisable from other "network problems". 
         *
         * If you want something that "feels" faster from pov of the user
         * and also gives you control over how to inform the user, then 
         * filtering responses (iptables --sport 53) on the OUTPUT chain 
         * allows us to rewrite the DNS. We can then send the user directly to 
         * 0.0.0.0 (and there is no timeout, e.g. a browser immediately says
         * page can't be loaded, and it looks less like a timeout that might be from 
         * other" network issues").
         * Using --rewrite-A-record <ipv4> and --rewrite--AAAA-record <ipv6> pointing 
         * to an internal ip address. There you might have a webserver serving a page and
         * describing to the user why the request was blocked, etc.
         */
        if (udp_hdr->dest == htons(cfg->port)) {
            // TODO check if QUERY is DNS question or answer
            LDEBUG("%s: Received UDP DNS QUESTION packet (%d -> %d)\n",
                    __func__, ntohs(udp_hdr->source), ntohs(udp_hdr->dest));

            if (find_punicode(pkt) != 0) {

                if (cfg->dryrun)
                    return nfq_set_verdict(q_handle, id, NF_ACCEPT, pkt_sz, pkt);

                pkt = NULL;
                return nfq_set_verdict(q_handle, id, NF_DROP, 0, pkt);
            }

        } else if (udp_hdr->source == htons(cfg->port)) {

        /* We are here because:
         * iptables -A OUTPUT -p udp -m udp --sport 53 -j NFQUEUE 
         *    --> E.g. we see UDP **response** packets (containing DNS answers)! 
         *
         * working on the response allows us to rewrite the DNS payload returned by 
         * the resolver. We can modify the response (answer) and inject our own ip.
         * E.g. 0.0.0.0., or send the client directly to an internal site hosting 
         * a short blurb describing what happend and why! 
         * Rewriting the packet to 0.0.0.0 then passing the modified payload with NF_ACCEPT 
         * back to the kernel (instead of dropping the packet with NF_DROP) is
         * the most user-friendly method. 
         */

            LDEBUG("%s: Received UDP packet (%d -> %d) with DNS answer in payload\n",
                    __func__, ntohs(udp_hdr->source), ntohs(udp_hdr->dest));

            if (find_punicode(pkt) != 0) {

                if (cfg->dryrun)
                    return nfq_set_verdict(q_handle, id, NF_ACCEPT, pkt_sz, pkt);

                if (cfg->rewrite_answer) {
                    unsigned int iphl;
                    //unsigned int udphl;
                    struct udphdr *udp_hdr_mangled = { 0 };
                    struct iphdr *ip_hdr_mangled = { 0 };
                    unsigned char *mangle_pkt = NULL;
                    int verdict;

                    mangle_pkt = (unsigned char*)xmalloc(PACKET_BUFSIZE*sizeof(char));
                    memset(mangle_pkt, 0, PACKET_BUFSIZE);
                    memcpy(mangle_pkt, pkt, pkt_sz);

                    mangle_pkt = mangle_packet(mangle_pkt, pkt_sz);

                    ip_hdr_mangled = (struct iphdr*)(mangle_pkt);
                    iphl = ip_hdr_mangled->ihl << 2;

                    udp_hdr_mangled = (struct udphdr*)(mangle_pkt + iphl);
                    //udphl = htons (iphl + sizeof(struct udphdr));

                    //udp_hdr_mangled->len = udphl;
                    udp_hdr_mangled->check=0;
                    //nfq_udp_compute_checksum_ipv4(udp_hdr_mangled, ip_hdr_mangled);
                    nfq_ip_set_checksum(ip_hdr_mangled);

                    LNOTICE("Sending modified DNS response payload (id=%d). "
                            "Verdict: NF_ACCEPT\n", id);

                    verdict = nfq_set_verdict(q_handle, id, NF_ACCEPT, pkt_sz, mangle_pkt);
                    xfree(mangle_pkt);

                    return verdict;
                }

                pkt = NULL;
                LNOTICE("Dropping DNS response (id=%d). Verdict: NF_DROP\n", id);
                return nfq_set_verdict(q_handle, id, NF_DROP, 0, pkt);
            }
        } // end response 
    } else {    // end UDP
        LERR("Not a UDP packet: Protocol [%d] not handled "
                "... misconfigured iptables?! Continuing with Verdict: NF_ACCEPT\n",
                ip_hdr->protocol);
        //return nfq_set_verdict(q_handle, id, NF_DROP, pkt_sz, pkt);
    }

    return nfq_set_verdict(q_handle, id, NF_ACCEPT, pkt_sz, pkt);
}

static list_t * load_whitelist(void)
{
    size_t len = 0;
    ssize_t read;
    FILE *fp;
    char *line = NULL;
    list_t * curr = NULL;
    list_t * whitelist = NULL;
    uint64_t i = 0;
    
    /* 
     * we loaded a whitelist previously, function called because of SIGHUP
     * remove struct of existing whitelist first
     */
    if (opt_whitelist_ptr != NULL) 
        list_t_free(opt_whitelist_ptr);

    // read whitelist into list
    fp = fopen(cfg->whitelist_path, "r");
    if (fp == NULL) {
        LDEBUG("Unable to open whitelist %s (%s)\n",
                cfg->whitelist_path, strerror(errno));
        return NULL;
    }

    LDEBUG("Loading allowed punicode/IDNs from whitelist ...\n");
    while ((read = getline(&line, &len, fp)) != -1) {
        char *p = line;

        // ignore comment lines
        // - skip all leading whitespace first
        while (*p == ' ' || *p == '\t') p++; 
        if (*p == '#') {
            //LDEBUG("%s: Ignoring comment line:\n\t-> %s", __func__, line);
            continue;  
        }

        // ignore empty new (standalone) lines
        if (*p == '\n') {
            //LDEBUG("%s Ignoring empty line:\n\t-> %s", __func__, line);
            continue;
        }
        
        if (i == 0) {
            whitelist = list_t_malloc();
            curr = whitelist;
        } else {
            curr->next = list_t_malloc();
            curr = curr->next; 
        }
        curr->str = strndup(line, strlen(line)-1);

        // clean up trailing new line
        if ((p = strchr(curr->str, '\n')) != NULL) *p = '\0';

        // ignore trailing whitespace and comments
        while (    (p = strrchr(curr->str, ' '))
                || (p = strrchr(curr->str, '\t')) 
                || (p = strrchr(curr->str, '#' )) ) {   
            // printf("Stripping trailing whitespace or comment markers in %s\n", curr->str);
            *p = '\0';
        } 

        LDEBUG("White-listing domain: %s", line);

        i++;
    }

    LINFO("White-listed total number of %lu domains\n", i);

    fclose(fp);
    if (line)
        xfree(line);

    return whitelist;
}

static int start_nfqueue_processing(void)
{
    int fd = 0;
    static struct nfq_handle *handle = { 0 };
    static struct nfnl_handle *nl_handle = { 0 };
    static struct nfq_q_handle *q_handle = { 0 };
    int ret = EXIT_SUCCESS;
    uint32_t queue_curr_len = QUEUE_MINLEN;
    uint32_t sock_curr_len = UDP_SOCK_RMEM_MIN;

    handle_ptr = handle = nfq_open();
    if (handle == NULL) {
        LERR("function nfq_open() returned error: %s\n",
                strerror(nfq_errno));
        return EXIT_FAILURE;
    }

    if (nfq_bind_pf(handle, AF_INET) < 0) {
        LERR("function nfq_bind_pf() returned error: %s\n",
            strerror(nfq_errno));
        nfq_close(handle);
        return EXIT_FAILURE;
    }

    q_handle_ptr = q_handle = nfq_create_queue(handle, cfg->queue_num, &logger_cb, NULL);
    if (q_handle == NULL) {
        if (errno == EPERM) {
            LERR("function nfq_create_queue() returned error: %s "
                    "- is another nfq process already handling queue number %d?\n",
                strerror(errno), cfg->queue_num);
        } else {
            LERR("function nfq_create_queue() returned error: %s\n",
                strerror(errno));
        }
        nfq_close(handle);
        return EXIT_FAILURE;
    }

    /*
     * the q_handle returned by nfq_create_queue() seems to have uninitialized
     * bytes resulting in a valgrind error:
     *
     * sudo valgrind --tool=memcheck --leak-check=yes --show-reachable=yes \
     *             --num-callers=20 --track-fds=yes --track-origins=yes -s \
     *             ./nfq --syslog --facility LOG_LOCAL0 --log-level info \
     *                   --port 53  --renice -20 --rewrite-answer
     *
     * ==714384==
     * ==714384== Syscall param socketcall.sendto(msg) points to uninitialised byte(s)
     * ==714384==    at 0x4B977C7: sendto (sendto.c:27)
     * ==714384==    by 0x486BE02: nfnl_send (in /usr/lib/x86_64-linux-gnu/libnfnetlink.so.0.2.0)
     * ==714384==    by 0x486DBD2: nfnl_query (in /usr/lib/x86_64-linux-gnu/libnfnetlink.so.0.2.0)
     * ==714384==    by 0x4A73995: nfq_set_mode (libnetfilter_queue.c:639)
     * ==714384==    by 0x10B247: start_nfqueue_processing (nfq.c:532)
     * ==714384==    by 0x10C289: main (nfq.c:987)
     * ==714384==  Address 0x1ffefefbfd is on thread 1's stack
     * ==714384==  in frame #3, created by nfq_set_mode (libnetfilter_queue.c:623)
     * ==714384==  Uninitialised value was created by a stack allocation
     * ==714384==    at 0x10A1B0: ??? (in /src/nfq/src/nfq)
     *
     * see also this post: https://marc.info/?l=netfilter-devel&m=137132916826745&w=4
     *
     */


    if (nfq_set_mode(q_handle, NFQNL_COPY_PACKET, 0xffff) < 0) {
        LERR("function nfq_set_mode() failed to set queue handle mode %s\n",
                strerror(errno));
        nfq_destroy_queue(q_handle);
        nfq_close(handle);
        return EXIT_FAILURE;
    }

    if (nfq_set_queue_maxlen(q_handle, queue_curr_len) < 0) {
        LERR("function nfq_set_queue_maxlen() returned error: %s "
                "- Unable to set kernel queue length\n",
                strerror(nfq_errno));
        nfq_destroy_queue(q_handle);
        nfq_close(handle);
        return EXIT_FAILURE;
    }

    if (nfq_set_queue_flags(q_handle, NFQA_CFG_F_UID_GID, NFQA_CFG_F_UID_GID)) {
        LERR("This kernel version does not allow us to "
                "retrieve process UID/GID: %s\n", strerror(errno));
        nfq_destroy_queue(q_handle);
        nfq_close(handle);
        return EXIT_FAILURE;
    }

    if (nfq_set_queue_flags(q_handle, NFQA_CFG_F_SECCTX, NFQA_CFG_F_SECCTX)) {
        fprintf(stderr, "This kernel version does not allow us to "
                "retrieve security context: %s\n", strerror(errno));
        nfq_destroy_queue(q_handle);
        nfq_close(handle);
        return EXIT_FAILURE;
    }

    nl_handle = nfq_nfnlh(handle);
    fd = nfnl_fd(nl_handle);

    LNOTICE("%s is now waiting for packets from NFQUEUE[%d] port[%d]\n",
            pgm, cfg->queue_num, cfg->port);

    while (1) {
        int recv_len = -1;
        char buf[PACKET_BUFSIZE] __attribute__ ((aligned));

        memset(buf, 0, sizeof(buf));
        if ((recv_len = recv(fd, buf, sizeof(buf), 0)) >= 0) {

            if (nfq_handle_packet(handle, buf, recv_len) < 0) {
                LDEBUG("%s: function nfq_handle_packet() returned -1: %s\n",
                        __func__, strerror(errno));
                continue;
            }
        }   
        /* if nfq is too slow to digest the packets sent from kernel-space, 
         * the socket buffer that we use to enqueue packets may fill up 
         * returning ENOBUFS. This error is logged and we try to mitigate 
         * with nfnl_rcvbufsiz() and nfq_set_queue_maxlen() until we reach
         * UDP_SOCK_RMEM_MAX and QUEUE_MAXLEN respectively.
         */
        if (recv_len < 0 && errno == ENOBUFS) {
            uint8_t x = 0;
            uint32_t success_rcvbufsiz = 0;

            LWARN("kernel queue is full (%s). %s is too slow and we are losing packets\n", 
                    strerror(errno), pgm);

            if (queue_curr_len < QUEUE_MAXLEN) {
                /* 
                 * Mitigation #1 - raise kernel queue size by means of nfq_set_queue_maxlen()
                 */
                LNOTICE("Trying to mitigate packet loss: "
                        "raise kernel queue size using nfq_set_queue_maxlen()\n");

                x = QUEUE_MAXLEN / queue_curr_len;
                switch(x) {
                    case 16:
                    case 8:
                    case 4:
                    case 2:
                        LNOTICE("Raising current kernel queue length from %d to %d\n",
                                queue_curr_len, queue_curr_len*2);

                        if (nfq_set_queue_maxlen(q_handle, queue_curr_len*2) < 0) {
                            LERR("function nfq_set_queue_maxlen() returned error: %s "
                                    "- Unable to increase kernel queue length\n",
                                    strerror(errno));
                            break;
                        }
                        queue_curr_len = queue_curr_len*2;
                        break;

                    default:
                        LERR("%s: BUG: Error calculating length of max queue size: "
                                "QUEUE_MAXLEN[%d]/queue_curr_len[%d] = x[%d]",
                                __func__, QUEUE_MAXLEN, queue_curr_len, x);
                        break;
                }

            } else {
                LDEBUG("%s: skipping nfq_set_queue_maxlen() mitigation for ENOBUFS "
                        "(it is already set to %d (max))\n", __func__, queue_curr_len);
            }

            if (sock_curr_len < UDP_SOCK_RMEM_MAX) {
                /* 
                 * Mitigation #2 - increase default socket read buf size by means of nfnl_rcvbufsiz(); 
                 */
                LNOTICE("Attempting to mitigate packet loss by increasing socket "
                        "read buf size using nfnl_rcvbufsiz()\n");

                x = UDP_SOCK_RMEM_MAX / sock_curr_len;
                switch(x) {

                    case 8:
                    case 4:
                    case 2:
                        LNOTICE("Raising current kernel socket buffer size from %d to %d\n",
                                sock_curr_len, sock_curr_len*2);

                        /* divide returned netlink_buffer_size by 2 since value passed to 
                         * kernel gets doubled in SO_RCVBUF; see net/core/sock.c */
                        success_rcvbufsiz = nfnl_rcvbufsiz(nl_handle, sock_curr_len/2);
                        if (success_rcvbufsiz != sock_curr_len) {
                            LERR("Setting socket min RECV buffer size failed (wanted=%u): %s\n", 
                                    sock_curr_len, strerror(nfq_errno));
                        } else {
                            sock_curr_len += success_rcvbufsiz;
                        }
                        break;

                    default:
                        LERR("%s: BUG: Error calculating value of max socket buffer size: "
                            "UDP_SOCK_RMEM_MAX[%d]/sock_curr_len[%d] = x[%d]",
                            __func__, UDP_SOCK_RMEM_MAX, sock_curr_len, x);
                        break;
                }
            } else {
                LDEBUG("%s: skipping nfnl_rcvbufsiz() mitigation for ENOBUFS "
                "(it is already set to %d (max))\n", __func__, sock_curr_len);
            }

             /* 
              * TODO: 
             * maybe provide cmd line option to set NETLINK_NO_ENOBUFS socket 
             * to avoid receiving ENOBUFS errors (requires Linux kernel >= 2.6.30)
             */
            continue;
        }   

        if (recv_len < 0) {
            LERR("Reading from nfq handle returned %d (%s)\n",
                    recv_len, strerror(errno));
            nfq_destroy_queue(q_handle);
            nfq_close(handle);
            ret = EXIT_FAILURE;
            break;
        }
        
    }
    return ret;
}


/*
 * signal handler
 * setup for SIGINT, SIGTERM & SIGHUP
 */
static void signal_handler(int signum)
{

    if (signum == SIGINT || signum == SIGTERM) {
        LWARN("Received signal number %d (%s) ... \n",
                signum,signum==SIGINT?"SIGINT":"SIGTERM");

        signal(SIGINT,SIG_IGN);
        signal(SIGTERM,SIG_IGN);

        print_stats();
        list_t_free(opt_whitelist_ptr);
        nfq_destroy_queue(q_handle_ptr);
        nfq_close(handle_ptr);

        LWARN("%s STOPPING! remember to flush iptables or permanently remove NFQUEUE "
                "firewall configuration if no longer useing %s)\n",pgm,pgm);

        opt_cfg_free(cfg);
        /* shut up valgrind --track-fds=yes :) */
        closelog();
        int i;
        for (i=0; i<3; i++) close(i);
        exit (EXIT_SUCCESS);

    } else if (signum == SIGHUP) {
        signal(SIGHUP,SIG_IGN);
        LWARN("Received SIGHUP: reloading whitelist %s\n",
                cfg->whitelist_path);

        opt_whitelist_ptr = load_whitelist();
        print_stats();
        signal(SIGHUP, &signal_handler); // reinstall
    }
    return;
}



int main(int argc, char **argv) 
{
    int index = 0, c = 0;
    int16_t ret = 0;
    uint8_t user_supplied_whitelist = 0;
    struct stat fbuf = { 0 };

    struct option long_options[] = {
        { "help",                no_argument,        NULL, 'h' },
        { "version",            no_argument,        NULL, 'v' },
        { "examples",            no_argument,        NULL, 'e' },
        { "syslog",                no_argument,        NULL, 's' },
        { "facility",            required_argument,    NULL, 'f' },
        { "dryrun",                no_argument,        NULL, 'd' },
        { "log-level",            required_argument,    NULL, 'l' },
        { "renice",                required_argument,    NULL, 'r' },
        { "queue-num",            required_argument,    NULL, 'q' },
        { "port",                required_argument,    NULL, 'p' },
        { "whitelist",            required_argument,    NULL, 'w' },
        { "rewrite-answer",    no_argument,            NULL, 'a' },
        { "rewrite-A-record",            required_argument,    NULL, '4' },
        { "rewrite-AAAA-record",        required_argument,    NULL, '6' },
        { NULL, 0, 0, 0 }
    };

    char *optstring = "hvesdfa:l:r:q:p:w:4:6:";

    pgm = basename(argv[0]);
    pid = (long)getpid();
    cfg = opt_cfg_malloc();

    opterr = 0;
    while ((c = getopt_long (argc, argv, optstring, long_options, &index)) != -1) {
        switch (c) {

            case 'h':
                usage(EXIT_SUCCESS);
            break;

            case 'v':
                version();
            break;

            case 'e':
                examples();
            break;

            case 's':
                cfg->syslog=1;
            break;

            case 'd':
                cfg->dryrun++;    
                break;

            case 'f':
                if (!(strcasecmp(optarg,"LOG_AUTH")) ) {
                    cfg->syslog_facility = LOG_AUTH;
                } else if (!(strcasecmp(optarg,"LOG_AUTHPRIV")) ) {
                    cfg->syslog_facility = LOG_AUTHPRIV;
                } else if (!(strcasecmp(optarg,"CRON")) ) {
                    cfg->syslog_facility = LOG_CRON;
                } else if (!(strcasecmp(optarg,"LOG_DAEMON")) ) {
                    cfg->syslog_facility = LOG_DAEMON;
                } else if (!(strcasecmp(optarg,"LOG_FTP")) ) {
                    cfg->syslog_facility = LOG_FTP;
                } else if (!(strcasecmp(optarg,"LOG_LOCAL0")) ) {
                    cfg->syslog_facility = LOG_LOCAL0;
                } else if (!(strcasecmp(optarg,"LOG_LOCAL1")) ) {
                    cfg->syslog_facility = LOG_LOCAL1;
                } else if (!(strcasecmp(optarg,"LOG_LOCAL2")) ) {
                    cfg->syslog_facility = LOG_LOCAL2;
                } else if (!(strcasecmp(optarg,"LOG_LOCAL3")) ) {
                    cfg->syslog_facility = LOG_LOCAL3;
                } else if (!(strcasecmp(optarg,"LOG_LOCAL4")) ) {
                    cfg->syslog_facility = LOG_LOCAL4;
                } else if (!(strcasecmp(optarg,"LOG_LOCAL5")) ) {
                    cfg->syslog_facility = LOG_LOCAL5;
                } else if (!(strcasecmp(optarg,"LOG_LOCAL6")) ) {
                    cfg->syslog_facility = LOG_LOCAL6;
                } else if (!(strcasecmp(optarg,"LOG_LPR")) ) { // ¯\_(ツ)_/¯
                    cfg->syslog_facility = LOG_LPR;
                } else if (!(strcasecmp(optarg,"LOG_MAIL")) ) {
                    cfg->syslog_facility = LOG_MAIL;
                } else if (!(strcasecmp(optarg,"LOG_NEWS")) ) {
                    cfg->syslog_facility = LOG_NEWS;
                } else if (!(strcasecmp(optarg,"LOG_USER")) ) {
                    cfg->syslog_facility = LOG_USER;
                } else if (!(strcasecmp(optarg,"LOG_UUCP")) ) {
                    cfg->syslog_facility = LOG_UUCP;
                } else {
                    printf("Error: unknown syslog facility specified with "
                            "--facility %s\n",optarg);
                    usage(EXIT_FAILURE);
                }
            break;

            case 'l':
                if (!(strcasecmp(optarg,"debug")) ) {
                    cfg->log_level = LOG_DEBUG;
                } else if (!(strcasecmp(optarg,"info")) ) {
                    cfg->log_level = LOG_INFO;
                } else if (!(strcasecmp(optarg,"notice")) ) {
                    cfg->log_level = LOG_NOTICE;
                } else if (!(strcasecmp(optarg,"warn")) ) {
                    cfg->log_level = LOG_WARNING;
                } else if (!(strcasecmp(optarg,"error")) ) {
                    cfg->log_level = LOG_ERR;
                } else {
                    printf("Error: unknown parameter in --log-level %s\n",optarg);
                    usage(EXIT_FAILURE);
                }
                break;

            case 'r':
                cfg->renice = atoi(optarg);    
                break;

            case 'q':
                cfg->queue_num = atoi(optarg);    
                break;

            case 'p':
                cfg->port = atoi(optarg);    
                break;

            case 'w':
                strlcpy(cfg->whitelist_path, optarg, sizeof(cfg->whitelist_path)-1);    
                user_supplied_whitelist++;
                break;

            case 'a':
                cfg->rewrite_answer++;
                break;

            case '4':
                memset(cfg->rewrite_ip_a_str, 0, sizeof(cfg->rewrite_ip_a_str));
                strlcpy(cfg->rewrite_ip_a_str, optarg, sizeof(cfg->rewrite_ip_a_str)-1);

                if (inet_pton(AF_INET, cfg->rewrite_ip_a_str, cfg->rewrite_ip_a) == 1) {
                    cfg->rewrite_answer++;
                    cfg->rewrite_A++;
                } else {
                    printf("Error: Option --rewrite-A-record %s requires a "
                            "valid ipv4 address: %s\n", cfg->rewrite_ip_a_str, 
                            strerror(errno));
                    usage(EXIT_FAILURE);
                }
                break;

            case '6':
                memset(cfg->rewrite_ip_aaaa_str, 0, sizeof(cfg->rewrite_ip_aaaa_str));
                strlcpy(cfg->rewrite_ip_aaaa_str, optarg, sizeof(cfg->rewrite_ip_aaaa_str)-1);

                if (inet_pton(AF_INET6, cfg->rewrite_ip_aaaa_str, cfg->rewrite_ip_aaaa) == 1) {
                    cfg->rewrite_AAAA++;
                    cfg->rewrite_answer++;
                } else {
                    printf("Error: Option --rewrite-AAAA-record %s requires a "
                            "valid ipv6 address: %s\n", cfg->rewrite_ip_aaaa_str, 
                            strerror(errno));
                    usage(EXIT_FAILURE);
                }
                break;

            case '?':
                if (optopt == 'q' || optopt == 'p' || optopt == 'w' 
                        || optopt == 'l' || optopt == 'r' || optopt == 'f' 
                        || optopt == '4' || optopt == '6') {
                    printf("Option -%c requires an argument.\n", optopt);
                } else if (isprint (optopt)) {
                    printf("Unknown option \'-%c\'.\n", optopt);
                } else {
                    printf("Unknown option character \'%x\'.\n",optopt);
                }
                usage(EXIT_FAILURE);
                break;

            default:
                usage(EXIT_FAILURE);
           }
    }

    for (index = optind; index < argc; index++) {
        printf("Non-option argument %s\n", argv[index]);
        usage(EXIT_FAILURE);
    }

    if (getuid() != 0) {
        printf("%s requires root (to access kernel/NFQUEUE)\n",pgm);
        return EXIT_FAILURE;
    }

    /* 
     * drop capabilities (in just ~3 lines of code :))
     * thanks RedHat! https://people.redhat.com/sgrubb/libcap-ng/
     */
    capng_clear(CAPNG_SELECT_BOTH);
    capng_updatev(CAPNG_ADD, CAPNG_EFFECTIVE|CAPNG_PERMITTED, 
        CAP_SYS_RESOURCE, 
        CAP_SYS_NICE, 
        CAP_NET_ADMIN, -1);
    capng_apply(CAPNG_SELECT_BOTH);

    // setup syslog ...
    if (cfg->syslog) {
        openlog(pgm, LOG_CONS|LOG_PID, cfg->syslog_facility);
        setlogmask (LOG_UPTO (cfg->log_level));
    }

    // are kernel modules loaded?
    if (stat("/proc/net/netfilter/nfnetlink_queue", &fbuf) == ENOENT) {
        LERR("Please ensure your kernel has the Netfilter QUEUE target built in, "
                "or loaded the nfnetlink_queue module.\n");
        opt_cfg_free(cfg);
        return EXIT_FAILURE;
    }

    opt_whitelist_ptr = load_whitelist();
    if (opt_whitelist_ptr == NULL) {

         if (user_supplied_whitelist) {
            LERR("Unable to find whitelist on user specified path %s -> Exiting!\n", 
                    cfg->whitelist_path);
            opt_cfg_free(cfg);
            return EXIT_FAILURE;
        } else {
            LNOTICE("No whitelist found in default path: %s "
                    "- Skipping whitelist processing\n",
                    cfg->whitelist_path);
        }
    }

    if (cfg->renice) {
        errno = 0; // man setpriority
        setpriority(PRIO_PROCESS, 0, cfg->renice);
        if (errno != 0) {
            LERR("Unable to renice. setpriority() returned: %s "
                    "(ignoring error and continue running with priority %d)\n",
                    strerror(errno), getpriority(PRIO_PROCESS, 0));
        } else {
            LNOTICE("%s process priority successfully set to %d\n",
                    pgm, getpriority(PRIO_PROCESS,0));
        }
    }

    /* install signal handler */
    signal(SIGINT, &signal_handler);
    signal(SIGTERM, &signal_handler);
    signal(SIGHUP, &signal_handler);

    if ((ret = start_nfqueue_processing()) != 0) {
        if (opt_whitelist_ptr) 
            list_t_free(opt_whitelist_ptr);

    }

    opt_cfg_free(cfg);
    closelog();

    /* valgrind */
    int i;
    for (i=0; i<3; i++) close(i);

    return ret;
}
