/* DNS payload parsing functions */

#include "nfq.h"

/* returns packet id */
extern uint32_t print_pkt (FILE *fp, struct nfq_data *tb)
{
    int id = 0;
    struct nfqnl_msg_packet_hdr *ph;
    struct nfqnl_msg_packet_hw *hwph;
    uint32_t mark, ifi, uid, gid;
    int ret;
    unsigned char *data, *secdata;

    if (fp == NULL) {
        LERR("%s Unable to write to file, fp == NULL\n",__func__);
        return -1;
    }
        
    fprintf(fp, "%s: --- START PACKET INFO --- \n",__func__);
    ph = nfq_get_msg_packet_hdr(tb);
    if (ph) {
        id = ntohl(ph->packet_id);
        fprintf(fp, "hw_protocol=0x%04x hook=%u id=%u ",
            ntohs(ph->hw_protocol), ph->hook, id);
    }

    hwph = nfq_get_packet_hw(tb);
    if (hwph) {
        int i, hlen = ntohs(hwph->hw_addrlen);

        printf("hw_src_addr=");
        for (i = 0; i < hlen-1; i++)
            fprintf(fp,"%02x:", hwph->hw_addr[i]);
        fprintf(fp,"%02x ", hwph->hw_addr[hlen-1]);
    }

    mark = nfq_get_nfmark(tb);
    if (mark)
        fprintf(fp,"mark=%u ", mark);

    ifi = nfq_get_indev(tb);
    if (ifi)
        fprintf(fp,"indev=%u ", ifi);

    ifi = nfq_get_outdev(tb);
    if (ifi)
        fprintf(fp,"outdev=%u ", ifi);
    ifi = nfq_get_physindev(tb);
    if (ifi)
        fprintf(fp,"physindev=%u ", ifi);

    ifi = nfq_get_physoutdev(tb);
    if (ifi)
        fprintf(fp,"physoutdev=%u ", ifi);

    if (nfq_get_uid(tb, &uid))
        fprintf(fp,"uid=%u ", uid);

    if (nfq_get_gid(tb, &gid))
        fprintf(fp,"gid=%u ", gid);

    ret = nfq_get_secctx(tb, &secdata);
    if (ret > 0)
        fprintf(fp,"secctx=\"%.*s\" ", ret, secdata);

    ret = nfq_get_payload(tb, &data);
    if (ret >= 0)
        fprintf(fp,"payload_len=%d ", ret);

    fprintf(fp,"\n--- END PACKET INFO --- \n");

    return id;
}
    

static char *get_url(unsigned char *query_ptr)
{
    int url_sz;
    char *url;

    // Get URL length
    url_sz = 0;
    while (query_ptr[url_sz]) {
        int label_sz = (unsigned int)query_ptr[url_sz];
        url_sz += label_sz + 1;
    }

    url = (char*)xmalloc(url_sz*sizeof(char));

    // Copy URL string
    url_sz = 0;
    while (query_ptr[url_sz]) {
        int label_sz = (int)query_ptr[url_sz];
        memcpy(
            url+url_sz,
            // Exclude the size byte
            query_ptr+url_sz+1,
            label_sz
        );
        url_sz += label_sz + 1;
    //    printf ("overwriting pos %d with . in url=%s\n",url_sz-1,url);
        url[url_sz-1] = '.';
    //    printf ("new url=%s\n",url);
    }
    url[url_sz-1] = '\0';

    return url;
}

static int get_query_len(unsigned char *query_ptr)
{
    int query_sz = 0;

    // URL field length
    while (query_ptr[query_sz]) {
        int label_sz = (int)query_ptr[query_sz];
        //LINFO("%s query_sz=%d + label_sz=%d +1\n", __func__, query_sz, label_sz);
        query_sz += label_sz + 1;
    }

    ++query_sz; /* Terminating zero byte of URL */

    query_sz += sizeof(q_ftr);

    return query_sz;

}

static unsigned char *get_dns_header(unsigned char *payload)
{
    //Use kernel iphdr/udphdr struct to parse the packet
    struct iphdr *ipHdr = (struct iphdr *)(payload);
    struct udphdr *udpHdr;

    if(ipHdr->version == 4) {
        udpHdr = (struct udphdr *)(payload + ((struct iphdr*)payload)->ihl * 4);
    } else {

        //struct ip6_hdr *ip6Hdr = (struct ip6_hdr *)payload;
        udpHdr = (struct udphdr *)(payload + sizeof(struct ip6_hdr));
    }
    return (unsigned char *)udpHdr + sizeof(struct udphdr);
}

//Get pointer to the start of url in the packet assuming there is only one question
//Note: Neither DjbDNS, BIND, nor MSDNS support queries where QDCOUNT > 1
static int get_query_url(unsigned char *url, unsigned char* dnsData)
{
    unsigned char *ptr = url;
    unsigned char l;
    int i, len = 0;

    //Convert the url hex data to string
    //03www05cisco03com -> www.cisco.com
    while(*dnsData != 0) {
        l = *dnsData;
        len += l+1;

        ++dnsData;

        for(i = 0; i < l; ++i, ++ptr, ++dnsData)
            *ptr = *dnsData;

        *ptr = '.';
        ++ptr;
    }
    *(--ptr) = '\0';

    return len + 1; //add the last 00 octet
}

#if 0
static int get_query_url_length(char* dnsData)
{
    unsigned char l;
    int i, len = 0;

    while(*dnsData != 0) {
        l = *dnsData;
        len += l+1;

        ++dnsData;

        for(i = 0; i < l; ++i, ++dnsData)
            ;
    }

    return len + 1; //add the last 00 octet
}
#endif 


unsigned char * mangle_packet(unsigned char *mangled_payload, int pkt_sz)
{
    unsigned char *dnsHdr; 
    unsigned char *dnsData;
    unsigned char *dnsAns;
    unsigned char url[512] __attribute__ ((aligned));
    int ansNum = 0, queryNameLen = 0, i;
    uint16_t type, dataLen;
    //unsigned char *mangled_payload = mngl_pkt;

    //mangled_payload = (unsigned char*)xmalloc(pkt_sz*sizeof(unsigned char)+1);
    //memset(mangled_payload,0,pkt_sz+1);
    //memcpy(mangled_payload, payload, pkt_sz);

    dnsHdr = get_dns_header(mangled_payload);
    dnsData = dnsHdr + sizeof(dnshdr);

    queryNameLen = get_query_url(url, dnsData);

    type = ntohs(*(uint16_t *)(dnsData + queryNameLen));

    dnsAns = dnsData + queryNameLen + 2 + 2; //Type and Class fields are both 2-byte
    //dataLen = ntohs(((dns_answer *)dnsAns)->len);

    /*
     * only handle DNS response with Answer RRs > 0 and query 
     * Type = A or Type = AAAA(0x001c)
     */
    ansNum = ntohs( ((dnshdr*)dnsHdr)->n_answer_rrs);
    if(ansNum > 0 && (type == 1 || type == 0x001c)) {

        LDEBUG("%s: query number in answer is %d\n", __func__,ansNum);
        LDEBUG("%s: dnsAns is %x\n", __func__, *dnsAns & 0xff);

        for(i = 0; i < ansNum; i++) {

            type = ntohs(((dns_answer *)dnsAns)->type);
            dataLen = ntohs(((dns_answer *)dnsAns)->len);

            if(type == 1) {

                unsigned char *ip = (unsigned char*)(dnsAns + sizeof(dns_answer));
                LNOTICE(";; Rewriting A record host address from %u.%u.%u.%u to %s\n",
                        ip[0], ip[1], ip[2], ip[3], cfg->rewrite_ip_a_str);

                for (int x=0; x<4; x++) 
                        ip[x]=cfg->rewrite_ip_a[x];


            } else if(type == 0x001c) {

                unsigned char *ip = (unsigned char*)(dnsAns + sizeof(dns_answer));
                LNOTICE(";; Rewriting AAAA record host address from "
                        "%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:"
                        "%02x%02x:%02x%02x:%02x%02x to ::/0\n",
                        ip[0], ip[1], ip[2], ip[3], ip[4], ip[5], ip[6], ip[7], ip[8], 
                        ip[9], ip[10], ip[11], ip[12], ip[13], ip[14], ip[15]);

                for (int x=0; x<16; x++) 
                        ip[x]=cfg->rewrite_ip_aaaa[x];

            } else {
                LDEBUG("%s: DNS Answer Type: %d (%2x) not handled\n",
                        __func__,type, ((dns_answer *)dnsAns)->type);
            }
            dnsAns += sizeof(dns_answer) + dataLen; 
        }
    } else {
        LDEBUG("%s: DNS query type is %d (%2x)", 
                __func__, type, type);
    }

    return mangled_payload;
}


query_list extract_urls(unsigned char *payload)
{
    uint16_t n_queries, i;
    unsigned char *query_ptr;
    dnshdr *dns_hdr;
    query_list qlist;

    dns_hdr = (dnshdr*)payload;

    n_queries = qlist.n_queries = ntohs(dns_hdr->n_questions);

    qlist.urls = (char**)xmalloc(n_queries*sizeof(char*));

    query_ptr = payload + sizeof(dnshdr);
    for (i = 0; i < n_queries; i++) {
        int len=0;
        qlist.urls[i] = (char*) get_url(query_ptr);
        len = get_query_len(query_ptr);
        query_ptr += len;
    }
    return qlist;
}

